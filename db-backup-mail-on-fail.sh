#!/bin/sh

# uncomment next line if you want to override some
# environment variables that should be available in subprocesses
#set -a

# path to the mailx binary
# default: mailx
MAILX_BIN_PATH="/usr/bin/mailx"


### helper functions ###

function error_exit() {
  EC=$?
  CMD_NAME="$(basename $0)"
  MSG="$CMD_NAME failed (exit code $EC)
$@"

  if [ -x "$MAILX_BIN_PATH" -a -n "$ERROR_LOG_EMAIL_TO" ]; then
    # send an email with the error message
    MAILX_EXTRA_ARGS="${ERROR_LOG_MAILX_EXTRA_ARGS:-}"
    if [ -n "$ERROR_LOG_EMAIL_FROM" ]; then
      MAILX_EXTRA_ARGS="$MAILX_EXTRA_ARGS -S from=\"$ERROR_LOG_EMAIL_FROM\""
    fi
    if [ -n "$ERROR_LOG_SMTP_URL" ]; then
      MAILX_EXTRA_ARGS="$MAILX_EXTRA_ARGS -S smtp=$ERROR_LOG_SMTP_URL"
    fi
    if [ -n "$ERROR_LOG_SMTP_USER" ]; then
      MAILX_EXTRA_ARGS="$MAILX_EXTRA_ARGS -S smtp-auth-user=$ERROR_LOG_SMTP_USER"
    fi
    if [ -n "$ERROR_LOG_SMTP_PW" ]; then
      MAILX_EXTRA_ARGS="$MAILX_EXTRA_ARGS -S smtp-auth-password=$ERROR_LOG_SMTP_PW"
    fi
    if [ -n "$ERROR_LOG_SMTP_USE_STARTTLS" ]; then
      case "$ERROR_LOG_SMTP_USE_STARTTLS" in
        1|yes|YES|true|TRUE)
          MAILX_EXTRA_ARGS="$MAILX_EXTRA_ARGS -S smtp-use-starttls"
          ;;
        *)
          echo "Invalid configuration value for ERROR_LOG_SMTP_USE_STARTTLS ($ERROR_LOG_SMTP_USE_STARTTLS)"
      esac
    fi

    echo "$MSG" | $MAILX_BIN_PATH  \
      -s "Database backup failure" \
      $MAILX_EXTRA_ARGS $CC "$ERROR_LOG_EMAIL_TO" || echo "Failed to send error log"
  fi
  echo "$MSG" 1>&2
  exit $EC
}

###

ERR_LOG="$(sh db-backup.sh 2>&1)" || error_exit "$ERR_LOG"
