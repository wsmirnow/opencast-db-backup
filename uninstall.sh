#!/bin/sh
set -e

systemctl disable db-backup.timer || true
rm -f /usr/local/bin/{db-backup.sh,db-backup-mail-on-fail.sh}
rm -f /usr/lib/systemd/system/{db-backup.service,db-backup.timer}
rm -f /etc/sysconfig/db-backup
rm -f /etc/systemd/system/db-backup.timer /etc/systemd/system/db-backup.service
rm -rf /etc/systemd/system/db-backup.timer/etc/systemd/system/db-backup.timer.d /etc/systemd/system/db-backup.service.d
systemctl daemon-reload
