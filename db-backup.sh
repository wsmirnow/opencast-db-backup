#!/bin/sh

# database host name or empty
# if not set or empty, the socket on the same system will be used
DB_HOST="${DB_HOST:-}"

# database port or empty
# if empty, default port (3306) will be used
DB_PORT="${DB_PORT:-}"

# protocol name may be TCP|SOCKET|PIPE|MEMORY
DB_PROTOCOL="${DB_PROTOCOL:-}"

# databse user for the connection
DB_USER="${DB_USER:-}"

# database password for the connection
DB_PW="${DB_PW:-}"

# database name
# this parameter is required
DB="${DB:-}"

# whitespace separated list of tables (without database name) that should not be backuped
# if empty, all tables will be backuped
DB_IGNORE_TABLES="${DB_IGNORE_TABLES:-}"

# backup output directory path
# this parameter is required
BACKUP_DIR="${BACKUP_DIR:-}"

# path to the mysqldump binary
# default: mysqldump
MYSQLDUMP_BIN_PATH="${MYSQLDUMP_BIN_PATH:-mysqldump}"

# path to the mysqlshow binary
# default: mysqlshow
MYSQLSHOW_BIN_PATH="${MYSQLSHOW_BIN_PATH:-mysqlshow}"

# additional arguments that should be pased to mysqldump command
# default: -n -t --single-transaction --dump-date
MYSQLDUMP_EXTRA_ARGS="${MYSQLDUMP_EXTRA_ARGS:- -n -t --single-transaction --dump-date}"

# path to the gzip binary
# default: gzip
GZIP_BIN_PATH="${GZIP_BIN_PATH:-gzip}"

# additional arguments that should be pased to gzip command
GZIP_EXTRA_ARGS="${GZIP_EXTRA_ARGS:-}"

##################################################
set -eu

### Check required parameters ###

: ${DB:?"Please set database name by passing DB=<db_name> environment variable."}
: ${BACKUP_DIR:?"Please set the backup output directory. That can be done by passing BACKUP_DIR=/path/to/backup/dir."}

### Create argument list for mysldump ###

BACKUP_ARGS=""
if [ -n "$DB_HOST" ]; then
  BACKUP_ARGS="$BACKUP_ARGS --host=$DB_HOST"
fi

if [ -n "$DB_PORT" ] && [ 0 -lt "$DB_PORT" ]; then
  BACKUP_ARGS="$BACKUP_ARGS --port=$DB_PORT"
fi

if [ -n "$DB_PROTOCOL" ]; then
  BACKUP_ARGS="$BACKUP_ARGS --protocol=$DB_PROTOCOL"
fi

if [ -n "$DB_USER" ]; then
  BACKUP_ARGS="$BACKUP_ARGS --user=$DB_USER"
fi

# if [ -n "$DB_PW" ]; then
#   #BACKUP_ARGS="$BACKUP_ARGS --password=$DB_PW"
#   export MYSQL_PWD="$DB_PW"
# fi

### Test database connection ###
# TABLES_OUT="/tmp/tbl.$$.$RANDOM"
# MYSQL_PWD="$DB_PW" $MYSQLSHOW_BIN_PATH $BACKUP_ARGS $DB 1>"$TABLES_OUT"
# TABLES_COUNT=$(($(cat $TABLES_OUT | wc -l)-4))
# echo "Database '$DB' has $TABLES_COUNT tables"
# rm -rf "$TABLES_OUT"

MYSQL_PWD="$DB_PW" $MYSQLSHOW_BIN_PATH $BACKUP_ARGS $DB 1>/dev/null
## The above command will terminate, if any connection parameter or database name is invalid ##

### Continuecreating argument list for mysqldump ###
BACKUP_ARGS="$BACKUP_ARGS --databases $DB"

for DB_IGNORE_TABLE in $DB_IGNORE_TABLES; do
  BACKUP_ARGS="$BACKUP_ARGS --ignore-table=$DB.$DB_IGNORE_TABLE"
done

mkdir -p "$BACKUP_DIR"
BACKUP_FILE_PATH="$BACKUP_DIR/$(date +'%F_%H%M%S')-$DB.sql.gz"

#echo "Start backup for database '$DB' on '${DB_HOST:-localhost}'"
MYSQL_PWD="$DB_PW" $MYSQLDUMP_BIN_PATH $BACKUP_ARGS $MYSQLDUMP_EXTRA_ARGS | gzip -c > "$BACKUP_FILE_PATH"
echo "Backup for database '$DB' done successfully. Output written to $BACKUP_FILE_PATH"
