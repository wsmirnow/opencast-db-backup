#!/bin/sh
set -eu

SERVICE_USER="root"
SERVICE_GROUP="root"

install -o root -g root -m 0644 -p -t /usr/local/bin/ db-backup.sh db-backup-mail-on-fail.sh
install -o root -g root -m 0644 -p -t /usr/lib/systemd/system/ systemd/db-backup.service systemd/db-backup.timer
install -o root -g "$SERVICE_GROUP" -m 0640 -p systemd/db-backup.conf /etc/sysconfig/db-backup

sed -i "s/User=.*/User=$SERVICE_USER/" /usr/lib/systemd/system/db-backup.service
sed -i "s/Group=.*/Group=$SERVICE_GROUP/" /usr/lib/systemd/system/db-backup.service

systemctl daemon-reload

cat <<- EOF
===== INFO =====
The service should be configured by user. The configuration file is located at /etc/sysconfig/db-backup.
After that you can enable the interval based database backup with:

  systemctl enable db-backup.timer

The service will then periodicly run every sunday at 3:45 AM. If you want to change it, run the following command:

  systemctl edit --full db-backup.timer

================
EOF
